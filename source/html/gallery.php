<!doctype html>
<html lang="de">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
		<title>Rylicx.at</title>
        
        <link rel="icon" type="/image/png" href="../../favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link rel="stylesheet" type="text/css" href="../css/overview.css">
        <link rel="stylesheet" type="text/css" href="../css/navbar.css">
        <script type="application/javascript" src="../js/navbar.js"></script>
	</head>
    <body>
        <div class='navbar' id="responsiveNavbar">
            <ul>
              <li><a href="../../ph-index.html">Home</a></li>
              <li><a href="tfp-shooting.php">TFP-Shooting</a></li>
              <li><a href="nature.php">Natur</a></li>
              <li><a href="aboutMe.html">Über mich</a></li>
              <a href="javascript:void(0);" class="icon" onclick="dropdownMenu(document)"></a>
            </ul>
        </div>  
        <div class="gallery-centerContent">
          <div class="gallery-grid-container">
            <?php
             $path = $_GET['path'];
             $pics = glob("../images/".$path.'/*');

              foreach($pics as $pic)
              {
                echo '<div class="gallery-grid-element">';
                echo '<div style="background-image: url(\''.$pic.'\');">';
                echo '</div>';
                echo '</div>';
              }
            ?>
          </div>
        </div>
	</body>
</html>