<!doctype html>
<html lang="de">
	<head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      <title>Rylicx.at</title>

      <link rel="icon" type="/image/png" href="../../favicon.ico">
      <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../css/style.css">
      <link rel="stylesheet" type="text/css" href="../css/overview.css">
      <link rel="stylesheet" type="text/css" href="../css/navbar.css">
      <script type="application/javascript" src="../js/navbar.js"></script>
	</head>
    <body>
        <div class='navbar' id="responsiveNavbar">
            <ul>
              <li><a href="../../ph-index.html">Home</a></li>
              <li><a href="tfp-shooting.php">TFP-Shooting</a></li>
              <li><a href="nature.php">Natur</a></li>
              <li><a href="aboutMe.html">Über mich</a></li>
            </ul>
        </div>  
        <div class="centerContent">
          <div class="grid-container">
             <?php
             $dirs = glob("../images/nature/*", GLOB_ONLYDIR);

              foreach($dirs as $dir)
              {
                $pics = glob($dir."/*");
                $dirName = explode("/", $dir);
                $picPrev = array_values($pics)[0];
                echo '<div class="grid-element">';
                echo '<a href="gallery.php?path='.$dirName[2]."/".$dirName[3].'" >';
                echo '<div style="background-image: url(\''.$picPrev.'\');">';
                echo '<div class="grid-element-box">';
                echo '<p>'.$dirName[3].'</p>';
                echo '</div>';
                echo '</div>';
                echo '</a>';
                echo '</div>';
              }
            ?>
          </div>
        </div>
	</body>
</html>