$(() =>{
  $('.image_slider').children('div').each((i,e)=>{
    $(e).css('background', 'url("source/images/slider/'+ (i+1) + '.jpg") no-repeat top');
    $(e).css('background-size', 'cover');
  });

  $('.image_slider div').click(onClickListener);
});



var onClickListener = function(e) {
  rotate($(e.currentTarget).parent(), $(e.currentTarget).index()-2);
}

function rotate(slider, direction) {
  if(direction > 0) {
    var first = slider.children('div').first();
    slider.children('div').first().remove();
    slider.append(first);
    slider.children('div').last().click(onClickListener);
    setTimeout( function() { rotate(slider, --direction);}, 300);
  }
  if(direction < 0) {
    var last = slider.children('div').last();
    slider.children('div').last().remove();
    slider.prepend(last);
    slider.children('div').first().click(onClickListener);
    setTimeout( function() { rotate(slider, ++direction);}, 300);
  }
}